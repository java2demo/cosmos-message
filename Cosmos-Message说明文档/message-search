message-search
===

> 对搜索引擎的一个封装，使使用起来更加方便。支持solr和Lucene。话说这个在OSC上被好多人给喷了。
> - [我封装的全文检索](http://www.oschina.net/code/snippet_151849_21445)
> - [我封装的全文检索之solr篇](http://my.oschina.net/sunhaojava/blog/133278)
> - [我封装的全文检索之lucene篇](http://my.oschina.net/sunhaojava/blog/132021)
> 
> 当然咯,每个人眼光不同,我水平可能比较次,写出的代码或许在比我牛逼的人看来,确实很挫,但是慢慢进步吗.取长补短,向每一个人学习.

1. 使用
    - 配置
    ```xml
    <!-- 搜索引擎使用solr -->
    <search:solr server="${search.solr.server}"/>
    <!-- 搜索引擎使用lucene -->
    <!--<search:lucene index="${search.lucene.index}"/>-->
    ```
    - 解释
        1. solr节点中得server配置的是solr服务器的地址
        2. lucene节点中得index配置本地索引文件路径
    - Java代码
        1. pojo对象
            > pojo对象还是按照正常的情况来写
        2. 对对象进行检索的配置,需要另外创建一个类,继承于SearchBean,提供`检索字段`、`索引字段`、`初始化`及`索引类型`
        ```java
        public class DemoSearchBean extends SearchBean {
            @Override
            public String[] getDoSearchFields() {
                // 返回检索到结果字段
                return new String[]{"name"};
            }

            @Override
            public String[] getDoIndexFields() {
                // 返回对哪些字段进行索引
                return new String[]{"name"};
            }

            @Override
            public void initPublicFields() throws Exception {
                // 初始化
                // 判断类型合法,后面可能需要修改,使用泛型
                Object obj = super.getObject();
                Demo demo = null;
                if(obj instanceof Demo)
                    demo = (Demo) obj;

                if(demo == null)
                    return;

                // 组装公共字段
                // 公共字段有 id、owerId、owerName、link、keyword、createDate
                // 其他字段由getDoIndexFields()方法提供
                Long pkId = demo.getPkId();
                super.setId(pkId.toString());
                super.setOwerId("1");
                super.setOwerName("孙昊");
                super.setLink("http://www.baidu.com");
                super.setKeyword("一个demo,id为" + pkId + ",name为" + demo.getName());
                super.setCreateDate(DateUtils.formatDate(new Date(), DateUtils.DEFAULT_PATTERN));
            }

            @Override
            public String getIndexType() {
                // 返回索引的类型，一般情况都是类名
                return this.getClass().getSimpleName();
            }
        }
        ```
        3. Java中的使用
        ```java
        // 1. 创建索引
        // 待创建索引的对象
        List<Demo> demos = testService.list();
        // 对象的索引描述对象
        List<SearchBean> searchVos = new ArrayList<SearchBean>(demos.size());
        for(Demo demo : demos){
            SearchBean sv = new DemoSearchBean();
            sv.setObject(demo);
            searchVos.add(sv);
        }
        // 创建索引
        this.searchEngine.doIndex(searchVos);

        // 2. 进行检索
        // 关键字
        String keyword = "keyword"
        // 创建检索对象,并设置关键字
        SearchBean bean = new DemoSearchBean();
        bean.setKeyword(name);
        // 进行检索,具体方法参数请参见代码的注释
        List beans = searchEngine.doSearch(bean, true, -1, -1).getItems();
        // 处理
        for(Object obj : beans){
            DemoSearchBean b = (DemoSearchBean) obj;
            // 公共字段的处理
            System.out.print("content: " + b.getKeyword() + " ");
            System.out.print("id: " + b.getId() + " ");
            System.out.print("owerId: " + b.getOwerId() + " ");
            System.out.print("owerName: " + b.getOwerName() + " ");
            System.out.print("link: " + b.getLink() + " ");
            // 这里是描述文件中提供的检索字段
            String[] doSearchFields = b.getDoSearchFields();
            // 上面对应字段检索得到的具体值
            Map<String, String> ev = b.getSearchValues();
            for(String f : doSearchFields){
                System.out.print(f + ": " + ev.get(f) + " ");
            }
            System.out.println("");
            System.out.println("===================================");
        }

        // 3. 删除索引
        SearchBean bean = new DemoSearchBean();
        // 设置创建索引时公共字段id的值
        bean.setId("1");
        searchEngine.deleteIndex(bean);

        // 4. 按照索引类型删除索引
        // 传入索引类型
        this.searchEngine.deleteIndexsByIndexType(DemoSearchBean.class);

        // 5. 删除所有的索引
        this.searchEngine.deleteAllIndexs();
        ```
2. solr的配置文件
```xml
<!-- start my solr -->
<field name="pkId" type="string" indexed="true" stored="true"/>
<field name="keyword" type="string" indexed="true" stored="true"/>
<field name="owerId" type="string" indexed="true" stored="true"/>
<field name="owerName" type="string" indexed="true" stored="true"/>
<field name="link" type="string" indexed="true" stored="true"/>
<field name="createDate" type="string" indexed="true" stored="true"/>
<field name="indexType" type="string" indexed="true" stored="true"/>
<!-- a dynamic field, match all fields what end with _message -->
<dynamicField name="*_message" type="paodingAnalyzer" indexed="true" stored="true"/>
<!-- end my solr -->
```
这里使用了`庖丁分词`,所以要配置`分词器`
```xml
<!-- paoding -->
<fieldType name="paodingAnalyzer" class="solr.TextField">  
   <analyzer class="net.paoding.analysis.analyzer.PaodingAnalyzer"></analyzer>  
</fieldType>
```

## 推荐一个查看索引的小软件
> 仅仅一个jar文件`lukeall-3.4.0_1.jar`,直接用`java -jar lukeall-3.4.0_1.jar`即可使用：[lukeall-3.4.0_1.jar（提取码：d3f2）](http://yunpan.cn/cwf9EuFG2NX7W)


##依赖的cosmos项目
- message-base
- message-utils
- message-template
- message-config

